# kleinanzeigen downloader



## Getting started
Copy the script to your folder and then just run it with python and the URL like:
python kleinanzeigen.py URL_OF_THE_AUCTION

## Name
Kleinanzeigen Downloader

## Description
This script is helping you to download your existing kleinanzeigen offers as once they expired you cannot open and reupload them again. All your data is more or less lost. :( 
So the script is creating a subfilder with the name of your item it creates a file with the same name, fetching, Title, Price and Description and saves it into a file you can always reopen.

After this it is downloading all imageds (up to 20) and also saves them into the folder

## Usage
python kleinanzeigen.py URL_OF_THE_AUCTION

## Support
No support provided

## Roadmap
No other ideas to improve (yet)

## Contributing
If you want to add some features or optimize the script feel free to submit, I will add it.

## License
As I am not a lover of GPL the MIT is the way to go :) So feel free to do what ever you want!

