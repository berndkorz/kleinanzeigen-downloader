# This script is helping you to download your existing kleinanzeigen offers 
# as once they expired you cannot open and reupload them again. All your data
# is more or less lost. :( 
# So the script is creating a subfilder with the name of your item
# it creates a file with the same name, fetching, Title, Price and Description and
# saves it into a file you can always reopen.
# After this it is downloading all imageds (up to 20) and also saves them into the folder

# This script is done as it is and without any legal checks or warrenties! The usage is fully 
# ON YOUR OWN RISC

import sys
import os
import requests
from bs4 import BeautifulSoup
import re
from urllib.parse import urlparse

def fetch_and_extract_data(url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
    }
    
    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # Raise an exception for unsuccessful responses
        
        soup = BeautifulSoup(response.content, 'html.parser')
        
        title_element = soup.find('h1', id='viewad-title')
        price_element = soup.find('h2', id='viewad-price')
        description_element = soup.find('p', id='viewad-description-text')
        image_elements = soup.find_all('div', class_='galleryimage-element')
        
        if title_element:
            title = title_element.get_text(strip=True)
        else:
            title = "Title element not found on the page."
        
        if price_element:
            price = price_element.get_text(strip=True)
        else:
            price = "Price element not found on the page."

        if description_element:
            description_html = str(description_element)
            description_text = re.sub(r'<br\s*/*>', '\n', description_html)
        else:
            description_text = "Description element not found on the page."
        
        image_urls = []
        for image_element in image_elements:
            img_tag = image_element.find('img')
            if img_tag:
                image_urls.append(img_tag['src'])
        
        return title, price, description_text, image_urls
    
    except requests.exceptions.RequestException as e:
        return f"An error occurred: {e}", "", "", []

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: myscript.py <URL>")
        sys.exit(1)
    
    url = sys.argv[1]
    parsed_url = urlparse(url)
    
    # Extract the desired part from the URL
    path_segments = parsed_url.path.strip('/').split('/')
    if len(path_segments) >= 2:
        folder_name = path_segments[-2]
    else:
        print("Invalid URL format")
        sys.exit(1)
    
    title, price, description_text, image_urls = fetch_and_extract_data(url)

    # Construct the folder name and filename
    folder_path = folder_name
    filename = f"{folder_name}.txt"
    
    # Extract the base filename without extension
    base_filename = os.path.splitext(filename)[0]
    
    # Create a folder with the same name as the base filename
    folder_path = base_filename
    os.makedirs(folder_path, exist_ok=True)
    
    # Construct the full file path within the created folder
    file_path = os.path.join(folder_path, filename)
    
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write(f"Title:\n{title}\n\nPrice:\n{price}\n\nDescription:\n{description_text}")
    
    print(f"Title, Price, and description saved to {file_path}")

    for index, image_url in enumerate(image_urls, start=1):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
        }
        image_response = requests.get(image_url, headers=headers)
        if image_response.status_code == 200:
            image_filename = os.path.join(folder_path, f"image_{index}.jpg")  # You can use a different file extension based on the actual image format
            with open(image_filename, 'wb') as image_file:
                image_file.write(image_response.content)
            print(f"Image {index} saved to {image_filename}")
        else:
            print(f"Failed to download image {index}.")
